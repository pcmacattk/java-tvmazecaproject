package ca5;

public class Utility
{
    public static String pad(String s, int size)
    {
        while(s.length() < size)
        {
            s = (char) 0 + s;
        }
        return s;
    }
    
    public static String removePadding(String s)
    {
        while(s.charAt(0) == (char)0)
        {
            s = s.substring(1);
        }
        return s;
    }
}
