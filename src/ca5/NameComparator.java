package ca5;

import java.util.Comparator;

/**
 *
 * @author shaqu_000
 */
public class NameComparator implements Comparator<Person>
{
    @Override
    public int compare(Person p1, Person p2)
    {
        
        return p1.getName().compareTo(p2.getName());
    }
}
