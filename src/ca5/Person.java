package ca5;

import java.util.ArrayList;
import java.util.Objects;

public class Person
{
    private double score;
    private String queryName;
    private String name;
    private int id;
    private ArrayList<String> imageURLs;
    private String personLink;
    private int myRating;
    private String myComments;
    
    public Person(double score, String queryName, String name, int id, ArrayList<String> imageURLs, String personLink, int myRating, String myComments)
    {
        this.score = score;
        this.queryName = queryName;
        this.name = name;
        this.id = id;
        this.imageURLs = imageURLs;
        this.personLink = personLink;
        this.myRating = myRating;
        this.myComments = myComments;
    }

    public double getScore()
    {
        return score;
    }

    public String getQueryName()
    {
        return queryName;
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }

    public ArrayList<String> getImageURLs()
    {
        return imageURLs;
    }

    public String getPersonLink()
    {
        return personLink;
    }

    public int getMyRating()
    {
        return myRating;
    }

    public String getMyComments()
    {
        return myComments;
    }

    public void setScore(double score)
    {
        this.score = score;
    }

    public void setQueryName(String queryName)
    {
        this.queryName = queryName;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setImageURLs(ArrayList<String> imageURLs)
    {
        this.imageURLs = imageURLs;
    }

    public void setPersonLink(String personLink)
    {
        this.personLink = personLink;
    }

    public void setMyRating(int myRating)
    {
        this.myRating = myRating;
    }

    public void setMyComments(String myComments)
    {
        this.myComments = myComments;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.score) ^ (Double.doubleToLongBits(this.score) >>> 32));
        hash = 97 * hash + Objects.hashCode(this.queryName);
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.imageURLs);
        hash = 97 * hash + Objects.hashCode(this.personLink);
        hash = 97 * hash + this.myRating;
        hash = 97 * hash + Objects.hashCode(this.myComments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (Double.doubleToLongBits(this.score) != Double.doubleToLongBits(other.score)) {
            return false;
        }
        if (!Objects.equals(this.queryName, other.queryName)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.imageURLs, other.imageURLs)) {
            return false;
        }
        if (!Objects.equals(this.personLink, other.personLink)) {
            return false;
        }
        if (this.myRating != other.myRating) {
            return false;
        }
        if (!Objects.equals(this.myComments, other.myComments)) {
            return false;
        }
        return true;
    }

    

    public String toHTMLTableData()
	{
		return "<td>" + this.score +"</td>"
                    + "<td>" + this.queryName +"</td>"
                    + "<td>" + this.name +"</td>"
                    + "<td>" + this.id +"</td>"
                    + "<td><image src =" + this.imageURLs.get(0) + ">"+"</td>"//for loop doesnt work.
                    + "<td>" + this.personLink +"</td>"
                    + "<td>" + this.myRating +"</td>"
                    + "<td>" + this.myComments +"</td>";
	}

    

    @Override
    public String toString()
    {
        return "Person{" + "score=" + score + ", queryName=" + queryName + ", name=" + name + ", id=" + id + ", imageURLs=" + imageURLs + ", personLink=" + personLink + ", myRating=" + myRating + ", myComments=" + myComments + '}';
    }
    
    
}