package ca5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class main
{
    static File f = new File("actors.dat");
    static Scanner in = new Scanner(System.in);
    private static String url = "http://api.tvmaze.com/search/people?q=";
    String key = "";
    static Map<String, Set<Person>> byQueryName = new HashMap<>();
    
    //INSTRUCTIONS : 1:SEARCH FOR PERSONS, THEN SAVE AND EXIT. RE-RUN PROGRAM AND REPEAT. 
    
    public static void displayMenu()
    {
        String indent = "";
        for(int i = 0; i < 10; i++)
        {
            indent += " ";
        }
        String[] items = {"1. Search for persons:", "2. List all persons:", "3. List persons sorted by person ID:", "4. List persons by rating:", "5. Edit person:", "6. Export search results to HTML file:", "7. Save and exit:"};
        for(int i = 0; i < items.length; i++)
        {
            System.out.println(items[i]);
        }
        System.out.print("");
    }
    
    public static void main(String[] args)
    {
        load();
        int choice = 0;
        while(choice != 7)
        {
            displayMenu();
            choice = in.nextInt();in.nextLine();
            switch(choice)
            {
                case 1:
                    searchPerson();
                    break;
                case 2:
                    listAllPersons();
                    break;
                case 3:
                    listPersonsSortedByID();
                    break;
                case 4:
                    listPersonsByRating();
                    break;
                case 5:
                    edit();
                    break;
                case 6:
                    export();
                    break;
                case 7:
                    saveAndExit();
                    break;
            }
        }
    }
    
    public static void load()
    {
        if(f.exists())
        {
            try
            {
                Set<Person> results = new HashSet<>();
                DataInputStream dis = new DataInputStream(new FileInputStream(f));
                while(dis.available() > 0)
                {
                    byte[] keyBytes = new byte[50];
                    dis.read(keyBytes);
                    String readkey = Utility.removePadding(new String(keyBytes));
                    
                    boolean readloop = true;
                    while(readloop == true) //readloop helps determine how many people there will be.
                    {
                        double score = dis.readDouble();

                        byte[] queryNameBytes = new byte[100];
                        dis.read(queryNameBytes);
                        String queryName = Utility.removePadding(new String(queryNameBytes));

                        byte[] nameBytes = new byte[50];
                        dis.read(nameBytes);
                        String name = Utility.removePadding(new String(nameBytes));

                        int id = dis.readInt();
                        
                        ArrayList<String> imageURLs = new ArrayList<String>();
                        for(int i = 0; i<2;i++)
                        {
                            byte[] imageURLsBytes = new byte[100];
                            dis.read(imageURLsBytes);
                            String newimage = Utility.removePadding(new String(imageURLsBytes));
                            imageURLs.add(newimage);
                        }
                        byte[] personLinkBytes = new byte[100];
                        dis.read(personLinkBytes);
                        String personLink = Utility.removePadding(new String(personLinkBytes));

                        int myRating = dis.readInt();

                        byte[] myCommentsBytes = new byte[100];
                        dis.read(myCommentsBytes);
                        String myComments = Utility.removePadding(new String(myCommentsBytes));

                        readloop = dis.readBoolean(); // stops duplicates, though when image = null theres an error/crash
                        
                        Person p = new Person(score, queryName, name, id, imageURLs, personLink, myRating, myComments);
                        results.add(p);
                    }
                    byQueryName.put(readkey, results);
                }
                dis.close();
            }
            catch (IOException ex)
            {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static void searchPerson()
    {
        //tvmaze code
        try 
        {
            Set<Person> people = new HashSet<>();
            
            System.out.println("Please enter search term for tvmaze");
            String searchTerm = in.nextLine();
            String encode = URLEncoder.encode(searchTerm, "UTF-8");
            System.out.println(encode);
            URL website = new URL(url + encode);
            InputStream siteIn = website.openStream();
            JsonReader reader = Json.createReader(siteIn);
            JsonArray allResults = reader.readArray();
            
            for(int i =0; i < allResults.size(); i++)
            {
                JsonObject obj = allResults.getJsonObject(i);
                //System.out.print(obj);//gives all the variables for the code down below.
                
                //Getting score
                JsonNumber score = obj.getJsonNumber("score");
                double peoplescore = score.doubleValue();
                
                //System.out.println(peoplescore);
                
                //declare jsonobj to get the details of the person
                JsonObject details = obj.getJsonObject("person");
                
                String name = details.getString("name");
                
                //System.out.println(name);
                
                int id = details.getInt("id");
                
                
                //getting the images, they have an original and medium size images.
                ArrayList<String> imageURLs = new ArrayList<String>();
                JsonObject pictures = details.getJsonObject("image");
                imageURLs.add(pictures.getString("medium"));
                imageURLs.add(pictures.getString("original"));
                
                //people's url
                String peopleurl = details.getString("url");
                
                Person person = new Person(peoplescore, url + encode, name, id, imageURLs, peopleurl, 1, "blank");
                people.add(person); // adding the person to the set
            }
            byQueryName.put(searchTerm, people); //adding the set to the hashmap
        } 
        catch (UnsupportedEncodingException ex) 
        {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void listAllPersons()
    {
        //System.out.println("ERROR1");
        ArrayList<Person> people = new ArrayList<Person>();
        Set<Person> printvalue = null;
        for(Map.Entry<String , Set<Person>> entry : byQueryName.entrySet()) //some of the code lines used in the method can be found at http://stackoverflow.com/questions/1066589/iterate-through-a-hashmap?rq=1 
        {
            String printkey = entry.getKey();
            printvalue = entry.getValue();
        } // adds the people into new values so they can be printed.
        Iterator<Person> iterator2 = printvalue.iterator(); //idea for code taken from http://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/ 
        while(iterator2.hasNext()) 
        {
            Person p = iterator2.next();
            people.add(p);
        }
        
        Collections.sort(people,new NameComparator());
            
        for(int i = 0; i < people.size(); i++)
        {
            System.out.println("People(Sorted)by name :" + people.get(i));
        }
    }
    
    
    private static void listPersonsSortedByID()
    {
        ArrayList<Person> people = new ArrayList<Person>();
        Set<Person> printvalue = null;
        for(Map.Entry<String , Set<Person>> entry : byQueryName.entrySet()) //code lines 214-217 found at http://stackoverflow.com/questions/1066589/iterate-through-a-hashmap?rq=1 
        {
            String printkey = entry.getKey();
            printvalue = entry.getValue();
        } // adds the people into new values so they can be printed.
        Iterator<Person> iterator2 = printvalue.iterator(); //idea for code taken from http://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/ 
        while(iterator2.hasNext()) 
        {
            Person p = iterator2.next();
            people.add(p);
        }
        
        Collections.sort(people,new IDComparator());
            
        for(int i = 0; i < people.size(); i++)
        {
            System.out.println("People(Sorted)by ID :" + people.get(i));
        }
    }
    
    private static void listPersonsByRating()
    {
        ArrayList<Person> people = new ArrayList<Person>();
        Set<Person> printvalue = null;
        for(Map.Entry<String , Set<Person>> entry : byQueryName.entrySet()) //code lines 214-217 found at http://stackoverflow.com/questions/1066589/iterate-through-a-hashmap?rq=1 
        {
            String printkey = entry.getKey();
            printvalue = entry.getValue();
        } // adds the people into new values so they can be printed.
        Iterator<Person> iterator2 = printvalue.iterator(); //idea for code taken from http://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/ 
        while(iterator2.hasNext()) 
        {
            Person p = iterator2.next();
            people.add(p);
        }
        
        Collections.sort(people,new IDComparator());
            
        for(int i = 0; i < people.size(); i++)
        {
            System.out.println("People(Sorted)by Rating :" + people.get(i));
        }
    }
    
    private static void edit()
    {
        
    }
    
    private static void export()
    {
        PrintWriter pWriter = null;
        try 
        {
            pWriter = new PrintWriter("People.html");
            pWriter.println("<html>");
            pWriter.println("<head><title>People</title></head>");
            pWriter.println("<body>");
            pWriter.println("<table border='1'>");
            pWriter.println("<tr><td>Score</td><td>QueryName</td><td>Name</td><td>id</td><td>ImageURLs</td><td>Person Link</td><td>Rating</td><td>Comments</td></tr>");
            
            ArrayList<Person> people = new ArrayList<Person>();
            Set<Person> printvalue = null;
            for(Map.Entry<String , Set<Person>> entry : byQueryName.entrySet()) //code lines 214-217 found at http://stackoverflow.com/questions/1066589/iterate-through-a-hashmap?rq=1 
            {
                String printkey = entry.getKey();
                printvalue = entry.getValue();
            } // adds the people into new values so they can be printed.
            Iterator<Person> iterator2 = printvalue.iterator(); //idea for code taken from http://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/ 
            while(iterator2.hasNext()) 
            {
                Person p = iterator2.next();
                people.add(p);
            }
            
            for(Person s : people)
            {
                pWriter.println("<tr>");
                pWriter.println(s.toHTMLTableData());
                pWriter.println("</tr>");
            }   pWriter.println("</table>");
            pWriter.println("</body>");
            pWriter.println("</html>");
            pWriter.close();
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally 
        {
            pWriter.close();
        }
	
    }
    
    private static void saveAndExit()
    {
        try
        {
            if(!f.exists())
            {
                f.createNewFile();
            }
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));
            Iterator<String> iterator = byQueryName.keySet().iterator();
            while(iterator.hasNext())
            {
                String firstkey = iterator.next();
                dos.writeBytes(Utility.pad(firstkey,50));
                Iterator<Person> iter = byQueryName.get(firstkey).iterator();
                while(iter.hasNext())
                {
                    Person p = iter.next();
                    dos.writeDouble(p.getScore());
                    dos.writeBytes(Utility.pad(p.getQueryName(), 100));
                    dos.writeBytes(Utility.pad(p.getName(), 50));
                    dos.writeInt(p.getId());
                    for(int i = 0; i<p.getImageURLs().size(); i++)
                    {
                        dos.writeBytes(Utility.pad(p.getImageURLs().get(i), 100));
                    }
                    dos.writeBytes(Utility.pad(p.getPersonLink(), 100));
                    dos.writeInt(p.getMyRating());
                    dos.writeBytes(Utility.pad(p.getMyComments(), 100));
                    
                    if(iter.hasNext()) //boolean is for the readloop at load(), 
                    {
                        dos.writeBoolean(true);
                    }
                    else
                    {
                        dos.writeBoolean(false);
                    }
                }
            }
            
            dos.close();
            
            System.exit(0);
            System.out.print("You have saved sucessfully. Goodbye! :)");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}