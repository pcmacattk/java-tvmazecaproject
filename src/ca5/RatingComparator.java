package ca5;

import java.util.Comparator;

/**
 *
 * @author shaqu_000
 */
public class RatingComparator implements Comparator<Person>
{
    @Override
    public int compare(Person p1, Person p2)
    {
        
        return p1.getMyRating() - p2.getMyRating();
    }
}
