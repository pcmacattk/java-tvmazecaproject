package ca5;

import java.util.Comparator;

/**
 *
 * @author shaqu_000
 */
public class IDComparator implements Comparator<Person>
{
    @Override
    public int compare(Person p1, Person p2)
    {
        
        return p1.getId() - (p2.getId());
    }
}
